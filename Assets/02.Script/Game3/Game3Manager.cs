﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Game3Manager : MonoBehaviour
{
    public GameObject KeyBoard;
    public enum State
    {
        PLAY,
        END
    }
    public State GameState;
    public static Game3Manager Instance;
    public GameObject ScoreBoard;
    public int Stage;

    public Text ScoreText;

    public Text StageText;

    public float RestTime;
    
    public void Awake()
    {
        Instance = this;
        GameState = State.PLAY;
    }

    public void ReActiveScoreBoard()
    {
        StartCoroutine(ReActive());
    }
    IEnumerator ReActive()
    {
        ScoreBoard.SetActive(false);
        yield return new WaitForSeconds(RestTime);

        ScoreBoard.SetActive(true);
    }
}
