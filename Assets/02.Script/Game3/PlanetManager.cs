﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetManager : MonoBehaviour
{
    public bool IsClone;
    void OnBecameInvisible() //화면밖으로 나가 보이지 않게 되면 호출이 된다.
    {
        if(IsClone)
            Destroy(this.gameObject); //객체를 삭제한다.
    }
   
}
