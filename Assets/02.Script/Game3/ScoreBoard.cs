﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Holoville;
using Holoville.HOTween;
using UnityEngine.UI;

public class ScoreBoard : MonoBehaviour
{
    
    public EaseType BoardEaseType;
    public float EasingDuration;
    public Vector3 StartPos;



    public int MoveIndex;
    public int MaxMoveIndex;

    public void Awake()
    {
        Game3Manager.Instance.Stage= 0;
        Game3Manager.Instance.StageText.text = Game3Manager.Instance.Stage.ToString();
        StartPos = transform.localPosition;
    }

    public void  OnEnable()
    {

        List<IHOTweenComponent> Tweens=  HOTween.GetAllTweens();
        for(int i =0; i< Tweens.Count; i++)
        {
            Tweens[i].Kill();
        }


        Game3Manager.Instance.StageText.text = Game3Manager.Instance.Stage.ToString();
        MoveIndex = 0;
        EasingDuration = EasingDuration - ((float)Game3Manager.Instance.Stage * 0.5f);
        transform.localPosition = StartPos;
        SetBack();
    }

    
    public void SetBack()
    {
        HOTween.To(this.transform, EasingDuration,
                   new TweenParms().Prop("localPosition", StartPos+ (Vector3.forward * -10)).Ease(BoardEaseType).OnComplete(SetUp));
    }
    public void SetUp()
    {
        MoveIndex += 1;
        if(MoveIndex== MaxMoveIndex)
        {
            //gameOver;
            Game3Manager.Instance.GameState = Game3Manager.State.END;
            this.gameObject.SetActive(false);
            Game3Manager.Instance.KeyBoard.SetActive(true);
            this.transform.localPosition = StartPos;

        }
        else
        {
            this.transform.localPosition = StartPos;
            Invoke("SetBack", 1.0f);

        }
    }


    void OnTriggerEnter(Collider col)
    {
        Destroy(col.gameObject);
        Game3Manager.Instance.Stage += 1;      
        Game3Manager.Instance.ScoreText.text = (   (int.Parse(Game3Manager.Instance.ScoreText.text)) +    MaxMoveIndex - MoveIndex).ToString();
        Game3Manager.Instance.ReActiveScoreBoard();
        this.gameObject.SetActive(false);
        
    }
}
