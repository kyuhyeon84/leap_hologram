﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leap.Unity.Interaction;
using Holoville.HOTween;
using UnityEngine.UI;

public class HandEventManager : MonoBehaviour
{


    [Header("Intro")]
    public static List<Transform> BtTrList;
    public static bool IsSelected = false;
    public IntroSelectBtProgressBar[] ProgressBars;
    public EaseType ProgressBarEaseType;
    public float ProgressBarEasingTime;
    public EaseType EndEaseType;
    public float EndEaseTypeEasingTime;
    private float[] _progressBarTime;

    public GameObject T_Linebreaker;
    public GameObject T_3dPuzzle;
    public GameObject T_EarthAttack;

   
    public float ProgressMaxTime;
    [Header("Main")]
    public float RotationDuration;  //Rotation Duration 회전 소요 시간
    #region Unity Method
    void Awake()
    {
        switch (ApplicationManager.Instance.State)
        {
            case AppState.INTRO:
                IsSelected = false;
                _progressBarTime = new float[ProgressBars.Length];
                BtTrList = new List<Transform>();
                break;
            case AppState.MAIN_1:
                break;
            case AppState.MAIN_2:
                break;
            case AppState.MAIN_3:
                break;
            case AppState.RESULT:
                break;
            default:
                break;
        }
    }
    void Update()
    {
        if (!IsSelected)
            switch (ApplicationManager.Instance.State)
            {
                case AppState.INTRO:
                    for (int i = 0; i < ProgressBars.Length; i++)
                    {
                        if (ProgressBars[i].IsTouching)
                        {
                            _progressBarTime[i] += Time.smoothDeltaTime;
                            if (_progressBarTime[i] > ProgressBarEasingTime)
                            {
                                float normalizedValue = (_progressBarTime[i] - ProgressBarEasingTime) / ProgressMaxTime;
                                SetProgressBar(i, normalizedValue);
                            }

                            if (_progressBarTime[i] > ProgressMaxTime + ProgressBarEasingTime)
                            {
                                Debug.Log("Selecting Complete!");
                                IsSelected = true;
                                EndIntro(i+1);
                            }
                        }
                    }
                    break;
                case AppState.MAIN_1:
                    break;
                case AppState.MAIN_2:
                    break;
                case AppState.MAIN_3:
                    break;
                case AppState.RESULT:
                    break;
                default:
                    break;
            }
    }
    #endregion


    #region Custom-Method
    #region UI-Transition : Set ProgressBar Stop & Start
    public void SetProgressBar(int index, bool isTouch) //Rotation Availability
    {
        ProgressBars[index].IsTouching = isTouch;
    }
    public void SetProgressBar(int index)   //Return
    {
        HOTween.To(ProgressBars[index].transform, ProgressBarEasingTime, new TweenParms().Prop("localRotation", new Vector3(0, 0, 0)).Ease(ProgressBarEaseType));
    }
    public void SetProgressBar(int index, float value)  //Fill Amount
    {
        ProgressBars[index].GetComponent<Image>().fillAmount = 1.0f - (value);
    }

    #endregion
    public void EndIntro(int sceneIndex)
    {
        for (int i = 0; i < ProgressBars.Length; i++)
        {
            HOTween.To(ProgressBars[i].GetComponent<RectTransform>(), EndEaseTypeEasingTime, new TweenParms().Prop("localScale", new Vector3(0, 0, 0)).Ease(EndEaseType));
            HOTween.To(BtTrList[i], EndEaseTypeEasingTime, new TweenParms().Prop("localScale", new Vector3(0, 0, 0)).Ease(EndEaseType));
        }

        StartCoroutine(FadeOutAndChangeScene(sceneIndex));
    }
    IEnumerator FadeOutAndChangeScene(int sceneIndex)
    {
        yield return new WaitForSeconds(EndEaseTypeEasingTime);
        ApplicationManager.Instance.UI2dController.FadeOut();
        yield return new WaitForSeconds(ApplicationManager.Instance.UI2dController.FadeDuration);
        ApplicationManager.Instance.LoadScene(sceneIndex);
    }
    

    #endregion
    public void RotateTween(GameObject obj, Vector3 rotation)
    {
        HOTween.To(obj.transform, RotationDuration, new TweenParms().Prop("localRotation", rotation).Ease(EaseType.EaseInQuad));
        //Obj(인자값)의 localratation을 rotation(인자값)으로 RotationDuration동안 회전시켜준다.
        //현업에서 Tween을 많이 사용함 - 트렌지션에 매우 용이함 - 해당 면을 빠른 시간 동안 눌렀다 땟을 때 열리다 마는 효과도 트윈 효과
    }

    #region LEAP-MOTION Event 
     
    public void OnPressKeyboard(string data)
    {
        Debug.Log(data);
        Keyboard.InputUserName(data);
    }
    
    public void OnPressStartPuzzle(GameObject obj)
    {

        TimeChecker.Instance.Init();
        Destroy(obj.GetComponent<InteractionBehaviour>());
        //InteractionBehaviour이 rigidbody를 의존하기 때문에 먼저 없애주어야 한다.
        Destroy(obj.GetComponent<Rigidbody>());
        Destroy(obj.GetComponent<BoxCollider>());

        //StartTimeCheck
        TimeChecker.Instance.IsStartPuzzle = true;

        PuzzleComponent.StartPuzzle();
        Invoke("StartDistanceCcalculating", 1.0f);

    }
    public void StartDistanceCcalculating()
    {
        PuzzleComponent.StartDistanceCcalculating();
    }

    public void OnPress(GameObject obj)
    {
        RotateTween(obj, new Vector3(0, 180, 0));
    }
    public void OnUnPress(GameObject obj)
    {
        RotateTween(obj, new Vector3(0, 0, 0));
    }
    public void OnPress(int index)  //메소드 오버로딩
    {
        if (!IsSelected)
        {
            SetProgressBar(index, true);
            SetProgressBar(index);
            
        }

        if (index == 0)
        {
            T_Linebreaker.SetActive(true);
            T_3dPuzzle.SetActive(false);
            T_EarthAttack.SetActive(false);
        }
        else if (index == 1)
        {
            T_Linebreaker.SetActive(false);
            T_3dPuzzle.SetActive(true);
            T_EarthAttack.SetActive(false);
        }
        else if (index == 2)
        {
            T_Linebreaker.SetActive(false);
            T_3dPuzzle.SetActive(false);
            T_EarthAttack.SetActive(true);
        }




    }
    public void OnUnPress(int index)  //메소드 오버로딩
    {
        if (!IsSelected)
        {
            SetProgressBar(index, false);
            SetProgressBar(index, 0.0f);
            _progressBarTime[index] = 0;
            
        }

        if (index == 0)
        {
            T_Linebreaker.SetActive(false);
            T_3dPuzzle.SetActive(false);
            T_EarthAttack.SetActive(false);
        }
        else if (index == 1)
        {
            T_Linebreaker.SetActive(false);
            T_3dPuzzle.SetActive(false);
            T_EarthAttack.SetActive(false);
        }
        else if (index == 2)
        {
            T_Linebreaker.SetActive(false);
            T_3dPuzzle.SetActive(false);
            T_EarthAttack.SetActive(false);
        }



    }

    #endregion
}
