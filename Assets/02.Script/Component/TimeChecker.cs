﻿using Holoville.HOTween;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeChecker : MonoBehaviour
{
    public static TimeChecker Instance;

    public const float DEAD_TIME = 30.0f;
    public float EndEaseTypeEasingTime;

    public GameObject PuzzleObj;

    public float PuzzleObEasingTime;
    public EaseType PuzzleObjEndEaseType;

    public GameObject KeyboardObj;

    public float KeyboardObjEasingTime;
    public EaseType KeyboardObjEndEaseType;

    public Vector3 EndScale;
    public EaseType EndEaseType;
    public Text TimeText;

    public bool IsStartPuzzle = false;
    public float SuccessTime;
    // Start is called before the first frame update
    void Awake()
    {
        if(Instance==null)
            Instance = this;     
        
    }
 
    public void Init()
    {
        SuccessTime = 0;
        PuzzleObj = GameObject.FindGameObjectWithTag("PuzzleObj");
    }
    public void InActive()
    {
        this.gameObject.SetActive(false);
    }

    public void EndPuzzle()
    {
        HOTween.To(transform, EndEaseTypeEasingTime, new TweenParms().Prop("localScale", EndScale).Ease(EndEaseType));
        HOTween.To(PuzzleObj.transform, PuzzleObEasingTime, new TweenParms().Prop("localScale", Vector3.zero).Ease(PuzzleObjEndEaseType).OnComplete(PopupKeyboard));
    }
    public void PopupKeyboard()
    {
        KeyboardObj.SetActive(true);
        HOTween.To(KeyboardObj.transform, KeyboardObjEasingTime, new TweenParms().Prop("localScale", Vector3.one).Ease(KeyboardObjEndEaseType));
    }
    // Update is called once per frame
    public void SetTimeText()
    {
        HOTween.To(transform, 0, new TweenParms().Prop("localScale", Vector3.one).Ease(EndEaseType));
        TimeText.text = (System.Math.Truncate(SuccessTime * 100) / 100).ToString();
    }

    void Update()
    {
        if (IsStartPuzzle)
        {
            SuccessTime += Time.smoothDeltaTime;
            TimeText.text = (System.Math.Truncate(SuccessTime * 100) / 100).ToString();
            if (SuccessTime > DEAD_TIME)
            {
                IsStartPuzzle = false;
            }
        }
    }
}
