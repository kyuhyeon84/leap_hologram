﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{

    public Text[] UserName;
    public Text[] UserScore;
    private void Awake()
    {
      
        this.gameObject.SetActive(false);
    }

    public void SetUserInfo(int index, string userName, float userScore)
    {

        UserName[index].text = userName;
        UserScore[index].text = userScore.ToString();
    }

    private void OnEnable()
    {
        if (Application.loadedLevelName == "ObjPuzzle")
        {
            for (int i = 0; i < 10; i++)
            {
                SetUserInfo(i, DataManager.UserDataList[i].UserName, DataManager.UserDataList[i].UserScore);
            }
        }else if (Application.loadedLevelName == "ThirdGame")
        {
            for (int i = 0; i < 10; i++)
            {
                SetUserInfo(i, DataManager.UserDataList3[i].UserName, DataManager.UserDataList3[i].UserScore);
            }
        }
        else if(Application.loadedLevelName== "LineBreaker")
        {
            for (int i = 0; i < 10; i++)
            {
                SetUserInfo(i, DataManager.UserDataList1[i].UserName, DataManager.UserDataList1[i].UserScore);
            }
        }
    }

}
