﻿using Holoville.HOTween;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Keyboard : MonoBehaviour
{

    public static TextMesh InputFieldTM;
    
    public float PopupEaseTypeEasingTime;
    public Vector3 PopupScale;
    public EaseType PopupEaseType;
    private void Start()
    {
        if (Application.loadedLevelName == "ObjPuzzle")
        {
            InputFieldTM = GameObject.FindGameObjectWithTag("InputField").GetComponent<TextMesh>();
            TimeChecker.Instance.KeyboardObj = this.gameObject;
            this.gameObject.SetActive(false);
        }
        else if (Application.loadedLevelName == "ThirdGame")
        {
            InputFieldTM = GameObject.FindGameObjectWithTag("InputField").GetComponent<TextMesh>();
            this.gameObject.SetActive(false);
        }
        else if (Application.loadedLevelName == "LineBreaker")
        {
            InputFieldTM = GameObject.FindGameObjectWithTag("InputField").GetComponent<TextMesh>();
            this.gameObject.SetActive(false);
        }
    }
    public static bool IsSave = false;
  
    public static void InputUserName(string key)
    {

        if(key =="Enter")
        {
            if (!IsSave)
            {
                IsSave = true;
                if (Application.loadedLevelName == "ObjPuzzle")
                {
                    DataManager.InputData();
                    DataManager.SortData();
                    ApplicationManager.Instance.UI2dController.FadeOut(0);
                }
                else if (Application.loadedLevelName == "ThirdGame")
                {
                    DataManager.UserScore = int.Parse(Game3Manager.Instance.ScoreText.text);

                    DataManager.InputData3();
                    DataManager.SortData3();
                    ApplicationManager.Instance.UI2dController.FadeOut(0);
                }
                else if (Application.loadedLevelName == "LineBreaker")
                {

                    DataManager.UserScore = Player.Instance.PlayerScore;
                    DataManager.InputData1();
                    DataManager.SortData1();
                    ApplicationManager.Instance.UI2dController.FadeOut(0);
                }
            }
        }
        else if(key =="back")
        {
            if (DataManager.UserName.Length > 0)
            {
                DataManager.UserName = DataManager.UserName.Substring(0, DataManager.UserName.Length - 1);
                InputFieldTM.text = DataManager.UserName;
            }
        }
        else
        {
            DataManager.UserName += key;
            InputFieldTM.text = DataManager.UserName;
        }
        
    }
   
    private void OnEnable()
    {
        IsSave = false;
        transform.localScale = Vector3.zero;
        HOTween.To(transform, PopupEaseTypeEasingTime, new TweenParms().Prop("localScale", PopupScale).Ease(PopupEaseType));
    }
}
