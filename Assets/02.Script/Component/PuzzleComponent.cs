﻿using Leap.Unity.Interaction;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleComponent : MonoBehaviour
{
    public const int PUZZLE_COUNT = 6;

    public static bool IsInit = false;
    public static List<Transform> TrList;
    public Vector3 InitPos;
    public Quaternion InitRotation;
    public static List<Rigidbody> RigidbodyList;
    public static List<MeshCollider> MeshColliderList;
    public static List<InteractionBehaviour> IBList;

    public static int AccumulatedIndex = 0;

    private bool _isPuzzleStart = false;
    #region Unity-Method
    private void Awake()
    {

        if (!IsInit)
        {
            AccumulatedIndex = 0;
            Debug.Log("Complete Init List");
            TrList = new List<Transform>();
            RigidbodyList = new List<Rigidbody>();
            MeshColliderList = new List<MeshCollider>();
            IBList = new List<InteractionBehaviour>();
           
        }
        IsInit = true;

        TrList.Add(transform);
        InitPos = transform.position;
        InitRotation = transform.rotation;
        RigidbodyList.Add(GetComponent<Rigidbody>());
        MeshColliderList.Add(GetComponent<MeshCollider>());
        IBList.Add(GetComponent<InteractionBehaviour>());
      

    }


    public static void StartPuzzle()
    {
        for (int i = 0; i < TrList.Count; i++)
        {
            MeshColliderList[i].enabled = true;
            RigidbodyList[i].isKinematic = false;
            IBList[i].enabled = true;
        }

    }
    public static void StartDistanceCcalculating()
    {
        for (int i = 0; i < TrList.Count; i++)
            TrList[i].GetComponent<PuzzleComponent>()._isPuzzleStart = true;
    }

    private void OnEnable()
    {

    }


    public Vector3 Pos;
    public Vector3 PrevPos;
    public Vector3 LandPos;
    public bool IsAcquirePos = false;
    
    // Update is called once per frame
    void Update()
    {
        if (_isPuzzleStart)
        {
           
            if (Vector3.Distance(transform.position, InitPos) < 0.04f)
            {
                _isPuzzleStart = false;
              Destroy(GetComponent<InteractionBehaviour>());

                GetComponent<MeshCollider>().enabled = false;
                GetComponent<Rigidbody>().isKinematic = true;
                transform.position = InitPos;
                transform.rotation = InitRotation;
                AccumulatedIndex += 1;
                if(AccumulatedIndex==PUZZLE_COUNT)
                {
                    DataManager.UserName = "";
                    DataManager.UserScore = TimeChecker.Instance.SuccessTime;
                    Debug.Log("Success All Puzzle");
                    TimeChecker.Instance.IsStartPuzzle = false;
                    TimeChecker.Instance.EndPuzzle();
                }
            }

            if(transform.position== PrevPos)
            {
                if(!IsAcquirePos)
                {
                    IsAcquirePos = true;
                    LandPos = transform.position;
                }
                
            }
            if(IsAcquirePos)
            {
                if (Vector3.Distance(transform.position, LandPos) > ReturnDistance)
                {
                    GetComponent<MeshCollider>().enabled = false;
                    transform.position = LandPos + Vector3.up*0.2f;
                    GetComponent<MeshCollider>().enabled = true;
                }
            }
            PrevPos = transform.position;
        }
    }
    #endregion
    public float ReturnDistance;
}
