﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Holoville.HOTween;

[RequireComponent(typeof(Transform))]
public class IntroSelectBt : MonoBehaviour
{
    public bool IsAlign;
    private void Start()
    {
        HandEventManager.BtTrList.Add(transform);
        if (IsAlign)
        {
            transform.LookAt(Camera.main.GetComponentsInChildren<Transform>()[1]);
          

        }
    }
}
