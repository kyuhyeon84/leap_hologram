﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Text.RegularExpressions;
public class ConfigUtil : MonoBehaviour
{

    #region Json Parsing
    // Load Config : Load Json File In Path
    // ref (일종의 포인터) : https://docs.microsoft.com/ko-kr/dotnet/csharp/language-reference/keywords/ref
    // T 제네릭 제네릭의 경우 처음 자료형만 명시해주면 컬렉션이나 오브젝트처럼 따로 형변환이 필요 없다 유동적으로 사용하기 위한 
    public static void LoadConfig<T>(string path, ref T t)
    {
        try
        {
            using (StreamReader sr = new StreamReader(path))
            {
                string json = sr.ReadToEnd();

                t = JsonUtility.FromJson<T>(json);
                Debug.Log(json);
                
                sr.Close();
            }
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);

        }

    }

    // Save Config : Save Json File In Path
    public static void SaveConfig<T>(string path, ref T t)
    {
        try
        {
            string json = JsonUtility.ToJson(t, prettyPrint: true);
            //For Json File Line Feed
            json = json.Replace(System.Environment.NewLine, "\n\r");
            Debug.Log(json);
            StreamWriter writer;
            var data = new FileInfo(path);
            if (!data.Exists)
            {
                writer = data.CreateText();
            }
            else
            {
                data.Delete();
                writer = data.CreateText();
            }

            foreach (var line in json.Split('\n'))
            {
                writer.WriteLine(line);
            }
            writer.Close();
            
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
            throw;
        }

    }
    #endregion


}
