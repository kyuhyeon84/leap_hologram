﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class ViewLayout : MonoBehaviour
{
    private IntPtr _processId;

    private const int HWND_TOPMOST = -1;
    private const int HWND_TOP = 0;
    private const int SWP_NOMOVE = 0x0002;
    private const int SWP_NOSIZE = 0x0001;

    private Vector2 _currentResolution;

    public float StartDelay = 1.0f;
    public bool DoAlwaysTop = true;
    

    [DllImport("user32.dll")]
    static extern bool SetForegroundWindow(IntPtr hWnd);

    [DllImport("USER32.DLL")]
    public static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

    [DllImport("user32.dll", EntryPoint = "SetWindowPos")]
    internal static extern int SetWindowPos(IntPtr hwnd, int hwndInsertAfter, int x, int y, int cx, int cy, int uFlags);

    [DllImport("user32.dll")]
    private static extern IntPtr GetActiveWindow();

    [DllImport("user32.dll")]
    static extern IntPtr SetWindowLong(IntPtr hwnd, int _nIndex, int dwNewLong);

  
    public void SetView()
    {
        if (!Application.isEditor)
        {
            _currentResolution = new Vector2(Screen.width, Screen.height);
            Screen.SetResolution(DataManager.ViewConfig.Width, DataManager.ViewConfig.Height, false);
            _processId = GetActiveWindow();
            RunEverySecond();
            Invoke("OnAfterDelay", StartDelay);
        }
    }
    void OnAfterDelay()
    {
        SetWindowBorderless();
        SetWindowFixedPosition();
    }

    void RunEverySecond()
    {
        UpdateAlwaysTop();  //항상 최상단 고정
        CheckResolution();  //해상도 틀어지는지 확인
        Invoke("RunEverySecond", 1.0f); //1초마다 매 확인
    }

    void CheckResolution()
    {
        if (_currentResolution.x != Screen.width || _currentResolution.y != Screen.height)
        {
            Debug.Log("Note : Resolution Changed.");
            SetWindowFixedPosition();

            _currentResolution.x = Screen.width;
            _currentResolution.y = Screen.height;
        }
    }

    void UpdateAlwaysTop()
    {
        if (DoAlwaysTop)
        {
            SetWindowPos(_processId, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
            SetForegroundWindow(_processId);
            ShowWindow(_processId, 9);
        }
    }

    private void SetWindowBorderless()
    {
        SetWindowLong(_processId, -16, 1);
    }

    private void SetWindowFixedPosition()
    {
        SetWindowPos(_processId, HWND_TOP, DataManager.ViewConfig.OffsetX, DataManager.ViewConfig.OffsetY, 
            DataManager.ViewConfig.Width, DataManager.ViewConfig.Height, (32 | 64));
    }
}
