﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UI2dComponent : MonoBehaviour
{
    public float Alpha
    {
        get
        {
            if (GetComponent<Image>() != null) return GetComponent<Image>().color.a;
            else if (GetComponent<RawImage>() != null) return GetComponent<RawImage>().color.a;
            else return 1.0f;
        }

        set
        {
            if (this.GetComponent<Image>() != null)
            {
                Color currColor = (this.GetComponent<Image>()).color;
                currColor.a = value;
                GetComponent<Image>().color = currColor;
            }
            else if (this.GetComponent<RawImage>() != null)
            {
                Color currColor = (this.GetComponent<RawImage>()).color;
                currColor.a = value;
                GetComponent<RawImage>().color = currColor;
            }
        }
    }
}
