﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : MonoBehaviour
{
    #region View-Config
    [System.Serializable]
    public class View
    {
        public int Width;
        public int Height;
        public int OffsetX;
        public int OffsetY;
    }
    #endregion
    public static View ViewConfig;
    public static string ViewConfigPath;
   
    
    #region Data-Config
    [System.Serializable]
    public class UserData
    {
        public string UserName;
        public float UserScore;
    }


    [System.Serializable]
    public class UserDatas
    {
        public UserData[] UserDataArray;
       
    }
    #endregion


    public static List<UserData> UserDataList1;

    public static UserDatas UserDatasConfig1;
    public static UserDatas UserDatasSaveConfig1;
    public static string UserDatasConfigPath1;


    public static List<UserData> UserDataList;

    public static UserDatas UserDatasConfig;
    public static UserDatas UserDatasSaveConfig;
    public static string UserDatasConfigPath;

    public static List<UserData> UserDataList3;

    public static UserDatas UserDatasConfig3;
    public static UserDatas UserDatasSaveConfig3;
    public static string UserDatasConfigPath3;


    public static string UserName;
    public static float UserScore;
    

    public static void InputData()
    {
        UserData user = new UserData();
        user.UserName = UserName;
        user.UserScore = UserScore;
        UserDataList.Add(user);

    }
    public static void InputData3()
    {
        UserData user = new UserData();
        user.UserName = UserName;
        user.UserScore = UserScore;
        UserDataList3.Add(user);
        Debug.Log("UserData3GameInput!");

    }
    public static void InputData1()
    {
        UserData user = new UserData();
        user.UserName = UserName;
        user.UserScore = UserScore;
        UserDataList1.Add(user);

    }
    public static void SortData1()
    {
        UserDataList1.Sort((DataManager.UserData a, DataManager.UserData b) => b.UserScore.CompareTo(a.UserScore)); //내림차순 정렬
        Debug.Log("UserData3Game SortingComplete!");
       
    }

    public static void SortData3()
    {
        UserDataList3.Sort((DataManager.UserData a, DataManager.UserData b) => b.UserScore.CompareTo(a.UserScore)); //내림차순 정렬
        Debug.Log("UserData3Game SortingComplete!");
        for(int i=0; i< UserDataList3.Count; i++)
        {
            Debug.Log(UserDataList3[i].UserScore);
        }
    }


    public static void SaveData1()
    {

        if (UserDataList1.Count < 10)
        {
            UserDatasSaveConfig1 = new UserDatas();
            Debug.Log("init UserDataSaveConfig1");
            UserDatasSaveConfig1.UserDataArray = new UserData[UserDataList1.Count];

            for (int i = 0; i < UserDataList1.Count; i++)
            {
                UserDatasSaveConfig1.UserDataArray[i] = new UserData();
                UserDatasSaveConfig1.UserDataArray[i].UserName = UserDataList1[i].UserName;
                UserDatasSaveConfig1.UserDataArray[i].UserScore = UserDataList1[i].UserScore;
            }
        }
        else
        {
            UserDatasSaveConfig1 = new UserDatas();
            Debug.Log("init UserDataSaveConfig1");
            UserDatasSaveConfig1.UserDataArray = new UserData[10];
            Debug.Log("init UserDataSaveConfig3Array");
            for (int i = 0; i < 10; i++)
            {
                UserDatasSaveConfig1.UserDataArray[i] = new UserData();
                UserDatasSaveConfig1.UserDataArray[i].UserName = UserDataList1[i].UserName;
                UserDatasSaveConfig1.UserDataArray[i].UserScore = UserDataList1[i].UserScore;
            }
        }

        ConfigUtil.SaveConfig<DataManager.UserDatas>(DataManager.UserDatasConfigPath1, ref DataManager.UserDatasSaveConfig1);
    }

    public static void SaveData3()
    {

        if (UserDataList3.Count < 10)
        {
            UserDatasSaveConfig3 = new UserDatas();
            Debug.Log("init UserDataSaveConfig3");
            UserDatasSaveConfig3.UserDataArray = new UserData[UserDataList3.Count];
            
            for (int i = 0; i < UserDataList3.Count; i++)
            {
                UserDatasSaveConfig3.UserDataArray[i] = new UserData();
                UserDatasSaveConfig3.UserDataArray[i].UserName = UserDataList3[i].UserName;
                UserDatasSaveConfig3.UserDataArray[i].UserScore = UserDataList3[i].UserScore;
            }
        }
        else
        {
            UserDatasSaveConfig3 = new UserDatas();
            Debug.Log("init UserDataSaveConfig3");
            UserDatasSaveConfig3.UserDataArray = new UserData[10];
            Debug.Log("init UserDataSaveConfig3Array");
            for (int i = 0; i < 10; i++)
            {
                UserDatasSaveConfig3.UserDataArray[i] = new UserData();
                UserDatasSaveConfig3.UserDataArray[i].UserName = UserDataList3[i].UserName;
                UserDatasSaveConfig3.UserDataArray[i].UserScore = UserDataList3[i].UserScore;
            }
        }

        ConfigUtil.SaveConfig<DataManager.UserDatas>(DataManager.UserDatasConfigPath3, ref DataManager.UserDatasSaveConfig3);
    }

    public static void SortData()
    {
        UserDataList.Sort((DataManager.UserData a, DataManager.UserData b) => a.UserScore.CompareTo(b.UserScore)); //오름차순 정렬
    }
    public static void SaveData2()
    {
       
        if (UserDataList.Count<10)
        {
            UserDatasSaveConfig = new UserDatas();
            UserDatasSaveConfig.UserDataArray = new UserData[UserDataList.Count];
            for (int i=0; i< UserDataList.Count;i++)
            {
                UserDatasSaveConfig.UserDataArray[i] = new UserData();
                UserDatasSaveConfig.UserDataArray[i].UserName = UserDataList[i].UserName;
                UserDatasSaveConfig.UserDataArray[i].UserScore = UserDataList[i].UserScore;
            }
        }else
        {
            UserDatasSaveConfig = new UserDatas();
            UserDatasSaveConfig.UserDataArray = new UserData[10];
            for (int i = 0; i < 10; i++)
            {
                UserDatasSaveConfig.UserDataArray[i] = new UserData();
                UserDatasSaveConfig.UserDataArray[i].UserName = UserDataList[i].UserName;
                UserDatasSaveConfig.UserDataArray[i].UserScore = UserDataList[i].UserScore;
            }
        }
        
        ConfigUtil.SaveConfig<DataManager.UserDatas>(DataManager.UserDatasConfigPath, ref DataManager.UserDatasSaveConfig);
    }
}
