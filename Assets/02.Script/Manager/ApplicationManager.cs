﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public enum AppState
{
    INTRO,
    MAIN_1,
    MAIN_2,
    MAIN_3,
    RESULT
}
/// <summary>
/// Scripting Order First Custom Class
/// </summary>

public class ApplicationManager : MonoBehaviour
{
    public static ApplicationManager Instance;
    public ViewLayout ViewManager;
    public UI2dManager UI2dController;
    public AppState State;
    public AsyncOperation Async;
    private string _dataPath;
    #region Unity-Method
    private void Awake()
    {
        if (Instance == null)   //프로그램 구동 되고 단 한번만
        {
            Instance = this;
            SetDataPath();  //경로 지정 
            DontDestroyOnLoad(this.gameObject);
            #region Read View-Setting
            DataManager.ViewConfigPath = string.Format("{0}{1}", GetDataPath(), "/config/ViewConfig.json");
            ConfigUtil.LoadConfig<DataManager.View>(DataManager.ViewConfigPath, ref DataManager.ViewConfig);
            #endregion



            #region Read UserData1-Setting
            DataManager.UserDatasConfigPath1 = string.Format("{0}{1}", GetDataPath(), "/config/UserDataConfig1.json");
            ConfigUtil.LoadConfig<DataManager.UserDatas>(DataManager.UserDatasConfigPath1, ref DataManager.UserDatasConfig1);
            #endregion

            #region Read UserData2-Setting
            DataManager.UserDatasConfigPath = string.Format("{0}{1}", GetDataPath(), "/config/UserDataConfig2.json");
            ConfigUtil.LoadConfig<DataManager.UserDatas>(DataManager.UserDatasConfigPath, ref DataManager.UserDatasConfig);
            #endregion

            #region Read Game3 UserData-Setting
            DataManager.UserDatasConfigPath3 = string.Format("{0}{1}", GetDataPath(), "/config/UserDataConfig3.json");
            ConfigUtil.LoadConfig<DataManager.UserDatas>(DataManager.UserDatasConfigPath3, ref DataManager.UserDatasConfig3);
            #endregion

            Invoke("InputUserLIst", 1.0f);//오브젝트 퍼즐 데이터 
            Invoke("InputUserLIst3", 1.0f);//세번째 게임 데이터
            Invoke("InputUserLIst1", 1.0f);//세번째 게임 데이터



            ViewManager.SetView();
            State = AppState.INTRO;
        }
    }


    public void InputUserLIst1()
    {

        DataManager.UserDataList1 = new List<DataManager.UserData>();


        for (int i = 0; i < DataManager.UserDatasConfig1.UserDataArray.Length; i++)
        {
            DataManager.UserDataList1.Add(DataManager.UserDatasConfig1.UserDataArray[i]);

        }
        DataManager.UserDataList1.Sort((DataManager.UserData a, DataManager.UserData b) => a.UserScore.CompareTo(b.UserScore)); //오름차순 정렬

      

    }

    public void InputUserLIst() 
    {

        DataManager.UserDataList = new List<DataManager.UserData>();


        for (int i = 0; i < DataManager.UserDatasConfig.UserDataArray.Length; i++)
        {
            DataManager.UserDataList.Add(DataManager.UserDatasConfig.UserDataArray[i]);

        }
        DataManager.UserDataList.Sort((DataManager.UserData a, DataManager.UserData b) => a.UserScore.CompareTo(b.UserScore)); //오름차순 정렬

      

    }
    public void InputUserLIst3()
    {

        DataManager.UserDataList3 = new List<DataManager.UserData>();


        for (int i = 0; i < DataManager.UserDatasConfig3.UserDataArray.Length; i++)
        {
            DataManager.UserDataList3.Add(DataManager.UserDatasConfig3.UserDataArray[i]);

        }
        DataManager.UserDataList3.Sort((DataManager.UserData a, DataManager.UserData b) => b.UserScore.CompareTo(a.UserScore)); //내림차순
     
    }
    private void Start()
    {

        
        UI2dController.FadeIn();

    }
    private void Update()
    {/*
        if(Async!=null)
            Debug.Log(Async.progress);
    */}

    #endregion

    #region Custom-Method
    #region 파일 여부
    public static bool IsExist(string filePath)
    {
        if (Directory.Exists(filePath)) return true;
        return File.Exists(filePath);

    }
    #endregion

    #region 데이터 경로 지정
    public void SetDataPath()
    {
        if (_dataPath == null)
        {
            string sameFolderPath = Directory.GetParent(Directory.GetCurrentDirectory()).FullName + @"\" + Application.productName + "Data";
            _dataPath = (IsExist(sameFolderPath)) ? sameFolderPath : null;
        }

        if (_dataPath == null)
        {
            _dataPath = Application.streamingAssetsPath;
        }

        Debug.Log("DataPath : " + _dataPath);
    }
    #endregion

    #region 데이터 경로 불러오기
    public string GetDataPath()
    {
        return _dataPath;
    }
    #endregion

    public void LoadScene(int sceneIndex)
    {
        StartCoroutine(LoadSceneAsyncInt(sceneIndex));
    }
    IEnumerator LoadSceneAsyncInt(int sceneIndex)
    {
        PuzzleComponent.IsInit = false;
        
        Async = Application.LoadLevelAsync(sceneIndex);
        yield return Async;
        Debug.Log("Loading complete");
        UI2dController.FadeIn();
        if (sceneIndex == 2)//나중에 추가할것 
        {
            UI2dController.TimeUIObj.SetActive(true);
        }


    }
    public void LoadScene(string sceneName)
    {
        StartCoroutine(LoadSceneAsyncString(sceneName));
    }
    IEnumerator LoadSceneAsyncString(string sceneName)
    {
        Async = Application.LoadLevelAsync(sceneName);
        yield return Async;
        Debug.Log("Loading complete");
        UI2dController.FadeIn();

    }
    #endregion





}
