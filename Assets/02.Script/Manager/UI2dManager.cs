﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Holoville.HOTween;
public class UI2dManager : MonoBehaviour
{

    public static Transform UICanvas;
    
    [Header("Dimed-Img")]
    public UI2dComponent FadeImg;
    [Header("Dimed-Type")]
    public EaseType FadeEaingType;
    [Header("Dimed-Duration")]
    public float FadeDuration;
    [Header("Time")]
    public GameObject TimeUIObj;
    [Header("Score")]
    public GameObject ScoreObj;

    public void Start()
    {
        if (UICanvas == null)
        {
            UICanvas = FadeImg.transform.parent.transform;
            DontDestroyOnLoad(UICanvas);
        }
        else
            FadeImg.transform.parent.gameObject.SetActive(false);
    }
    public void FadeOut()
    {
        HOTween.To(FadeImg, FadeDuration, new TweenParms().Prop("Alpha", 1).Ease(FadeEaingType));
    }
    public void FadeOut(int end)
    {
        HOTween.To(FadeImg, FadeDuration, new TweenParms().Prop("Alpha", 1).Ease(FadeEaingType).OnComplete(OnScoreBoard));
    }
    public void OnScoreBoard()
    {

        if (Application.loadedLevelName == "ObjPuzzle")
        {
            TimeChecker.Instance.SuccessTime = 0;
            TimeChecker.Instance.SetTimeText();
            TimeChecker.Instance.InActive();
            ScoreObj.SetActive(true);
            Invoke("SaveDataAndEnd2", 1.0f);
            Invoke("EndScene", 4.0f);
        }
        else if (Application.loadedLevelName == "ThirdGame")
        {
           
            ScoreObj.SetActive(true);
            Invoke("SaveDataAndEnd3", 1.0f);
            Invoke("EndScene", 4.0f);
        }
        else if (Application.loadedLevelName=="LineBreaker")
        {
            ScoreObj.SetActive(true);
            Invoke("SaveDataAndEnd1", 1.0f);
            Invoke("EndScene", 4.0f);
        }
    }
    public void SaveDataAndEnd2()
    {
        DataManager.SaveData2();
    }
    public void SaveDataAndEnd3()
    {
        DataManager.SaveData3();
    }
    public void SaveDataAndEnd1()
    {
        DataManager.SaveData1();
    }
    public void EndScene()
    {
        ScoreObj.SetActive(false);
        
        ApplicationManager.Instance.LoadScene(0);
    }

    public void FadeIn()
    {
        HOTween.To(FadeImg, FadeDuration, new TweenParms().Prop("Alpha", 0).Ease(FadeEaingType));
    }
    

}
