﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardManager : MonoBehaviour {

    public phyllotaxis[] _phyllotaxis;
    public float[] _degreeSet;
     int _degreeIndex = 0;
    // Use this for initialization
    void Start () {
       

    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.Space))
        {
            _degreeIndex++;
            if (_degreeIndex == _degreeSet.Length)
                _degreeIndex = 0;
            for(int i =0; i<_phyllotaxis.Length;i++)
            {
                _phyllotaxis[i]._degree = _degreeSet[_degreeIndex];
            }
            
        }
		
	}
}
