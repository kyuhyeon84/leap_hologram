﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckCollider : MonoBehaviour
{
    public GameObject Circle;
    public GameObject Index;

    #region OnTrigger
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject == Index)
        {
            SoundPlayer.Instance.PlaySound("CircleOn", 1);
            Circle.SetActive(true);
        }
    }
    void OnTriggerStay(Collider col)
    {
        if (col.gameObject == Index)
        {
            Vector3 lookVec = Circle.transform.position;
            lookVec += Vector3.forward;
            Circle.transform.LookAt(lookVec);
        }
    }
    void OnTriggerExit(Collider col)
    {
        if (col.gameObject == Index)
            Circle.SetActive(false);

    }
    #endregion

    #region UnityMethod
    void OnDisable()
    {
        Circle.SetActive(false);
    }
    void OnEnable()
    {
        Circle.SetActive(false);
    }
    #endregion



}
