﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct SoundData
{
    public AudioClip Audio;
    public string Name;
}

[RequireComponent(typeof(AudioSource))]
public class SoundPlayer : MonoBehaviour
{
    public static SoundPlayer Instance;

    void Awake()
    {
        Instance = this;
        _audio = gameObject.GetComponent<AudioSource>();
    }

    public List<SoundData> SoundList = new List<SoundData>();
    private AudioSource _audio;

    public void PlaySound(string audioName, float volume = 1.0f)
    {
        for (int i = 0; i < SoundList.Count; i++)
        {
            if (SoundList[i].Name == audioName)
            {
                _audio.PlayOneShot(SoundList[i].Audio, volume);
                break;
            }
        }
    }
}

