﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour
{
    public GameObject EnemyMaker;

    public void OnEnemyMaker()
    {
        Debug.Log("on");
        EnemyMaker.SetActive(true);
    }

    public void OffEnemyMaker()
    {
        Debug.Log("off");
        EnemyMaker.SetActive(false);
    }
}
