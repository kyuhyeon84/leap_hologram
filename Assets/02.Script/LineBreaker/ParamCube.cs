﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParamCube : MonoBehaviour
{

    //band index
    public int _band;
    public float _startScale, _scaleMultiplier;
    public bool _useBuffer;
    public float Data;
    public Color MainColor;
    public Color EmissionColor;
    Material _material;
    // Use this for initialization
    void Start()
    {
        _material = GetComponent<MeshRenderer>().materials[0];
    }

    // Update is called once per frame
    void Update()
    {

        Data = (AudioData.FreqBand[_band] * _scaleMultiplier) * _startScale;
        transform.localScale = new Vector3(transform.localScale.x, Data, transform.localScale.z);
       

        MainColor = new Color(Random.Range(0, 1.0f), Random.Range(0, 1.0f), Random.Range(0, 1.0f));
        EmissionColor = MainColor * AudioData.FreqBand[_band];
        _material.SetColor("_Color", MainColor);
        _material.SetColor("_EmissionColor", MainColor * AudioData.FreqBand[_band]);


    }
}
