﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(AudioSource))]
public class AudioData : MonoBehaviour
{
    
    public float[] Sample = new float[512];
    public static float[] FreqBand = new float[8];
    private AudioSource _audioSource;
    void Start()
    {
        _audioSource = GetComponent<AudioSource>();
    }

   
    void Update()
    {
        GetSpectrumAudioSource();
        MakeFrequencyBands();
        if(_audioSource.isPlaying)
        {

            float ratio = _audioSource.time / _audioSource.clip.length;
            Player.Instance.InputAudioTime(ratio);
            if(ratio>0.99f)
            {
                EnemyMaker.Instance.StopGame();
            }


        }
    }

    void MakeFrequencyBands()
    {
        /*
		 * 22050 / 512 = 43hertz per sample
		 * 
		 * 20-60 hertz
		 * 60-250 hertz
		 * 250-500 hertz
		 * 500-2000 hertz
		 * 2000-4000 hertz
		 * 4000-6000 hertz
		 * 6000-20000hertz
		 * 임의로 지정해준 것 최대한 위의 주파수에 가깝게 끊기 위해서 
		 * 0-2 = 43*2 = 86herz = 0~ 43*2(86)
		 * 1-4 = 43*4 = 172 hertz 87-258  
		 * 2-8 = 344 hertz 259-602
		 * 3-16 = 688 hertz 603-1290
		 * 4-32 = 1376 hertz 1291-2666
		 * 5-64 = 2752 hertz 2667-5418
		 * 6-128 = 5504 hertz 5419-10922
		 * 7-256 = 11008 hertz 10923-21930
		 * 510
		 */
        int count = 0;
        for (int i = 0; i < FreqBand.Length; i++)
        {
            float average = 0;
            int sampleCount = (int)Mathf.Pow(2, i) * 2; //2 4 8 16 32 64 128 256 
            if (i == 7)
            {
                sampleCount += 2;
            }

            //새로운 주파수 대역을 만들기 위해서 
            for (int j = 0; j < sampleCount; j++)
            {
               
                average += Sample[count] * (count + 1);    
                count++;   

            }

            //count -> 0~1 / 2~5 / 6~13 / 14~ 29  .......  Max 511 total 0~511

            average /= count;
          
            FreqBand[i] = average * 10;//10은 임의의 수ㅁ
        }


    }
    void GetSpectrumAudioSource()
    {
        _audioSource.GetSpectrumData(Sample, 0, FFTWindow.Blackman);
    }
}
