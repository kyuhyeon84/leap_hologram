﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyMaker : MonoBehaviour {

    public static EnemyMaker Instance;

    public GameObject MusicEq;
    public ParamCube[] ParamCubes;
    public GameObject[] Prefab;
    public Text CountDownText;
    public GameObject KeyboardObj;
    private IEnumerator _makerCoroutine;
    
    void Awake()
    {
        Instance = this;
        this.gameObject.SetActive(false);
    }

    void OnEnable()
    {
        _makerCoroutine = MakeEnemy();
        StartCoroutine(_makerCoroutine);
    }
    public void StopGame()
    {
        if(_makerCoroutine!=null)
            StopCoroutine(_makerCoroutine);
        Invoke("DelayActiveKeyboard", 3.0f);
    }
    public void DelayActiveKeyboard()
    {
        KeyboardObj.SetActive(true);

    }

    IEnumerator MakeEnemy()
    {

        SoundPlayer.Instance.PlaySound("CountDown", 1);
        for(int i=5; i>0; i--)
        {
            CountDownText.text = i.ToString();
            yield return new WaitForSeconds(1.0f);
        }
        CountDownText.text = "Game Start!";
       MusicEq.SetActive(true);
        yield return new WaitForSeconds(1.0f);
        CountDownText.text = "";
        while (true)
        {
            
            for(int i=0; i< Prefab.Length; i++)
            {
                GameObject Enemy = Instantiate(Prefab[i], this.transform);
                float data=0;
                int index = 0;
                for(int j =0; j < ParamCubes.Length; j++)
                {


                    if ( ParamCubes[j].Data > data)
                    {
                        data = ParamCubes[j].Data;
                        index = j;
                    }
                }
         
                Enemy.transform.position = ParamCubes[index].transform.position;
                Enemy.transform.position = new Vector3(Enemy.transform.position.x, 0, Enemy.transform.position.z);
              
                Enemy.transform.LookAt(GameObject.FindGameObjectWithTag("Player").transform);
                Enemy.GetComponent<Renderer>().material.SetColor("_Color", ParamCubes[index].MainColor);
                Enemy.GetComponent<Renderer>().material.SetColor("_EmissionColor", ParamCubes[index].EmissionColor);
                Enemy.GetComponent<Enemy>().FilledMat = Enemy.GetComponent<Renderer>().material;
                
                yield return new WaitForSeconds(1.0f);
            }

          
        }
    }
}
