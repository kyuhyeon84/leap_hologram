﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour {

    public static int Score = 10;
    public static float MoveSpeed= 9.0f;
    public bool IsCut = false;
    public TextMeshPro BlockScoreText;
    
    public Material FilledMat;
    public float _maxDistance;
    public float _distance;
    private GameObject _cutObj;
   
    void OnTriggerEnter(Collider col)
    {
        if(!IsCut)
        if(col.gameObject.name== "LineCollider")
            {
                Player.Instance.InputScore((int)(Score * _distance));
                SoundPlayer.Instance.PlaySound("Shot", 1);
                IsCut = true;
                Destroy(this.GetComponent<Collider>());
                GameObject[] objs=MeshCut.Cut(this.gameObject, transform.position, Vector3.Cross(col.transform.parent.GetComponent<LineRenderer>().GetPosition(1), col.transform.parent.GetComponent<LineRenderer>().GetPosition(0)), FilledMat);
                //(col.transform.parent.GetComponent<LineRenderer>().GetPosition(1) - col.transform.parent.GetComponent<LineRenderer>().GetPosition(0)).normalized, FilledMat);
                for (int i =0; i<objs.Length; i++)
                {
                   if(objs[i].name== "rightSide")
                    {
                        _cutObj = objs[i];
                    }

                   if(objs[i].GetComponent<Rigidbody>()==null)
                        objs[i].AddComponent<Rigidbody>();
                    objs[i].AddComponent<BoxCollider>();
                    objs[i].GetComponent<Rigidbody>().useGravity = true;
                }
        }

    }
   
    void OnEnable()
    {
        BlockScoreText = GetComponentInChildren<TextMeshPro>();
        
        _maxDistance=Vector3.Distance(Player.Instance.transform.position, transform.position);
        Invoke("Disable", 10.0f);
       
    }
    void Disable()
    {
        if (_cutObj != null)
            Destroy(_cutObj);
        Destroy(this.gameObject);
    }
    // Update is called once per frame
    void Update()
    {
        if (!IsCut)
        {
            _distance = Vector3.Distance(Player.Instance.transform.position, transform.position)/ _maxDistance;
            BlockScoreText.text = ((int)(Score * _distance)).ToString();
            transform.Translate(Vector3.forward * Time.smoothDeltaTime * MoveSpeed);
        }
    }
}
