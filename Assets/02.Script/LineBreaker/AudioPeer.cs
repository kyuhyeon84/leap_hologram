﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(AudioSource))]
public class AudioPeer : MonoBehaviour {

	AudioSource _audioSource;
	public float[] _audioBand = new float[512];


    //https://youtu.be/4Av788P9stk
    // Use this for initialization
    void Start () {
	
		_audioSource = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		GetSpectrumAudioSource ();
	}

	void GetSpectrumAudioSource()
	{
		_audioSource.GetSpectrumData (_audioBand, 0, FFTWindow.Blackman);
	}
}
