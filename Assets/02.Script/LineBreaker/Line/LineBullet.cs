﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineBullet : MonoBehaviour {


    public Vector3 Direction;
    public float Speed = 5;
    public float DeadTime = 3;
    public IEnumerator Routine;
    public void OnEnable()
    {
        
    }
    public void StartMoving()
    {
        Routine = MoveBullet();
        StartCoroutine(Routine);
        Invoke("DeatObj", DeadTime);
    }
    IEnumerator MoveBullet()
    {
        while(true)
        {
            transform.Translate(Direction * Time.smoothDeltaTime * Speed);
       
            yield return null;
        }
    }
    public void DeatObj()
    {
        Destroy(this.gameObject);
    }
}
