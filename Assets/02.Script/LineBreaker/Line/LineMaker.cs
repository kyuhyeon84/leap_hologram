﻿using Leap.Unity.Interaction;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineMaker : MonoBehaviour {



    public GameObject AimingObj;
    public LineRenderer Lr;
    public GameObject[] Circles;
    public float LineWidth;

    public float RestDuration;

    private bool _isActive = false;
    private bool E_isActive = true;


    private bool isActive;
    private bool isActivePrev;
	// Use this for initialization
	void Start () {

        Lr.SetWidth(LineWidth, LineWidth);
        isActive = false;
        isActivePrev = false;
        Lr.gameObject.SetActive(false);
        _isActive = false;
        
    }

    // Update is called once per frame

    public GameObject Line;

	void Update () {

        if (Application.loadedLevelName == "LineBreaker")
        {

            isActive = Circles[0].activeSelf && Circles[1].activeSelf;

            if (isActive)
            {
                if (!isActivePrev)
                {
                    AimingObj.SetActive(true);
                    Lr.gameObject.SetActive(true);
                    Line = Instantiate(Lr.gameObject);
                }


                Line.GetComponent<LineRenderer>().SetPosition(0, Circles[0].transform.position);
                Line.GetComponent<LineRenderer>().SetPosition(1, Circles[1].transform.position);
                float angle = Mathf.Atan2((Circles[1].transform.position.z - Circles[0].transform.position.z), (Circles[1].transform.position.x - Circles[0].transform.position.x));
                float angle2 = Mathf.Atan2((Circles[1].transform.position.y - Circles[0].transform.position.y), (Circles[1].transform.position.x - Circles[0].transform.position.x));
                AimingObj.transform.position = Vector3.zero;
                AimingObj.transform.rotation = Quaternion.identity;
                angle *= Mathf.Rad2Deg;
                angle2 *= Mathf.Rad2Deg;
                angle *= -1;
                AimingObj.transform.Rotate(new Vector3(0, angle, angle2));
                AimingObj.transform.Translate(Vector3.forward * 5);
            }
            else
            {
                if (isActivePrev)
                {
                    AimingObj.SetActive(false);
                    AddColliderToLine(Line.GetComponent<LineRenderer>(), Line.GetComponent<LineRenderer>().GetPosition(0), Line.GetComponent<LineRenderer>().GetPosition(1));

                    Line.GetComponent<LineBullet>().Direction = (AimingObj.transform.position - Lr.transform.position).normalized;
                    Line.GetComponent<LineBullet>().StartMoving();
                    SoundPlayer.Instance.PlaySound("Line", 1);
                    Lr.gameObject.SetActive(false);
                }

            }
            isActivePrev = isActive;
        }
        else
        {
            if (Game3Manager.Instance.GameState == Game3Manager.State.PLAY && !_isActive)
            {
                isActive = Circles[0].activeSelf && Circles[1].activeSelf;

                if (isActive && E_isActive)
                    //손가락을 집었을때
                {
                    if (!isActivePrev)
                    {
                        planet.SetActive(true);
                    }
                    Vector3 Circle1Pos = Circles[0].transform.position;
                    Vector3 Circle2Pos = Circles[1].transform.position;
                    planet.transform.position = (Circle1Pos + Circle2Pos) / 2.0f;
                    //planet 손가락 중간에 지구본 생성

                    float distance = Vector3.Distance(Circles[0].transform.position, Circles[1].transform.position);
                    //손가락 사이의 거리를 변수로 지정

                    planet.transform.localScale = new Vector3(distance, distance, distance) * Coefficient;
                    //planet의 크기를 손가락의 거리에 따라 지구본의 크기를 달리함
                }
                else if(!isActive && E_isActive)
                {
                    if (isActivePrev)
                        //집었다가 떼었을때
                    {

                        GameObject planetClone = Instantiate(planet, planet.transform.position, Quaternion.identity, null);
                        //planet클론 생성
                        planetClone.transform.localScale = planet.transform.localScale;
                        planet.SetActive(false);
                        planetClone.GetComponent<SphereCollider>().enabled = true;
                        planetClone.GetComponent<InteractionBehaviour>().enabled = true;
                        planetClone.GetComponent<PlanetManager>().IsClone = true;
                        //planet 클론 material 복사
                        _isActive = true; //빠른 시간내에 중복 생성 방지
                        E_isActive = false;

                        StartCoroutine(DelayAndActive());
                    }

                }
                isActivePrev = isActive;
            }
        }

	}
    
    IEnumerator DelayAndActive()
    {
        yield return new WaitForSeconds(RestDuration);
        _isActive = false;
        E_isActive = true;

    }


    #region ThirdGame
    public GameObject planet;
    public float Coefficient;
    #endregion


    public Vector3 DirVec;
    private void AddColliderToLine(LineRenderer line, Vector3 startPoint, Vector3 endPoint)
    {
        //create the collider for the line
        BoxCollider lineCollider = new GameObject("LineCollider").AddComponent<BoxCollider>();
        //set the collider as a child of your line
        lineCollider.transform.parent = line.transform;
        // get width of collider from line 
        float lineWidth = line.endWidth;
        // get the length of the line using the Distance method
        float lineLength = Vector3.Distance(startPoint, endPoint);
        // size of collider is set where X is length of line, Y is width of line
        //z will be how far the collider reaches to the sky
        lineCollider.size = new Vector3(lineLength, lineWidth, 0.1f);
        // get the midPoint
        Vector3 midPoint = (startPoint + endPoint) / 2;
        // move the created collider to the midPoint
        lineCollider.transform.position = midPoint;


        //heres the beef of the function, Mathf.Atan2 wants the slope, be careful however because it wants it in a weird form
        //it will divide for you so just plug in your (y2-y1),(x2,x1)
        float angle = Mathf.Atan2((endPoint.z - startPoint.z), (endPoint.x - startPoint.x));
        float angle2 = Mathf.Atan2((endPoint.y - startPoint.y), (endPoint.x - startPoint.x));
     

        // angle now holds our answer but it's in radians, we want degrees
        // Mathf.Rad2Deg is just a constant equal to 57.2958 that we multiply by to change radians to degrees
        angle *= Mathf.Rad2Deg;
        angle2 *= Mathf.Rad2Deg;
        //were interested in the inverse so multiply by -1
        angle *= -1;
      
        // now apply the rotation to the collider's transform, carful where you put the angle variable
        // in 3d space you don't wan't to rotate on your y axis
        lineCollider.transform.Rotate(0, angle, 0);
        lineCollider.transform.Rotate(0, 0, angle2);
       
        
       
    }
}
