﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public static Player Instance;
    public Text PlayerScoreText;
    public Slider AudioSlider;
    public int PlayerScore;
    

    private void Awake()
    {
        Instance = this;
        PlayerScoreText.text = "0";
    }
    public void InputAudioTime(float normalizedValue)
    {
        AudioSlider.value = normalizedValue;
    }

    public void InputScore(int score)
    {
        PlayerScore += score;
        PlayerScoreText.text = PlayerScore.ToString();
    }
}
